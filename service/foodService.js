const BASE_URL = "https://62b07878196a9e9870244798.mockapi.io/mon-an";

export let foodService = {
  getFoodList: () => {
    return axios({
      url: BASE_URL,
      method: "GET",
    });
  },

  delFood: (idMonAn) => {
    return axios({
      url: `${BASE_URL}/${idMonAn}`,
      method: "DELETE",
    });
  },

  addNewFood: (monan) => {
    return axios({
      url: BASE_URL,
      method: "POST",
      data: monan,
    });
  },

  getFoodInfo: (idMonAn) => {
    return axios({
      url: `${BASE_URL}/${idMonAn}`,
      method: "GET",
    });
  },

  foodUpdate: (food) => {
    return axios({
      url: `${BASE_URL}/${food.id}`,
      method: "PUT",
      data: food,
    });
  },
};
